package section1_intro.part1_language_basics;


public class Point {
    int x;
    int y;

    double euclideanDistanceTo(Point otherPoint) {
        //calculate distance - can you implement this?
        return Math.sqrt(Math.pow(otherPoint.x - this.x,2) + Math.pow(otherPoint.y - this.y,2));
    }
}
