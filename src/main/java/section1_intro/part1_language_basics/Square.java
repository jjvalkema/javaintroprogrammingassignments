package section1_intro.part1_language_basics;

public class Square {
    Point upperLeft;
    Point lowerRight;

    /**
     * returns the surface defined by the rectangle with the given upper left and lower right corners.
     * It assumes two corners have been created already!
     * @return
     */
    int surface(){
        //calculate surface - can you implement this?\
        int result =((this.lowerRight.x - this.upperLeft.x ) * (this.upperLeft.y - this.lowerRight.y ) );
        System.out.println(result);
        return (result );
    }
}
