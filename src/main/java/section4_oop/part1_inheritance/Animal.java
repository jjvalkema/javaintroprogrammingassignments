/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package section4_oop.part1_inheritance;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Animal {
    private String name;
    private String movementType;
    private double speed;
    private double max_speed;
    private double max_age;
    private double age;

    public Animal(String name,String movementType,double max_speed, double max_age, double age) {
        setName(name);
        setMovementType(movementType);
        setMax_speed(max_speed);
        setMax_age(max_age);
        setAge(age);
        setSpeed();
    }

    /**
     * returns the name of the animal
     * @return name the species name
     */
    public String getName() {
        return name;
    }
    
    /**
     * returns the movement type
     * @return movementType the way the animal moves
     */
    public String getMovementType() {
        return movementType;
    }

    /**
     * returns the speed of this animal
     * @return speed the speed of this animal
     */
    public double getSpeed() {
        return this.speed;
    }

    public void setSpeed() {
        this.speed = this.max_speed * (0.5 + (0.5 * ((this.max_age - this.age) / this.max_age)));
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getMax_speed() {
        return max_speed;
    }

    public void setMax_speed(double max_speed) {
        this.max_speed = max_speed;
    }

    public double getMax_age() {
        return max_age;
    }

    public void setMax_age(double max_age) {
        this.max_age = max_age;
    }

    public double getAge() {
        return age;
    }

    public void setAge(double age) {
        this.age = age;
    }

}
