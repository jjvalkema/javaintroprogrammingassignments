/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package section4_oop.part1_inheritance;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Tortoise extends Animal {

    public Tortoise(String name, String movementType, double max_speed, double max_age, double age) {
        super(name, movementType, max_speed, max_age, age);
    }
}
